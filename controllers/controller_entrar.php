<?php

class Controller_Entrar extends Controller
{
    
    function __construct()
    {
        $this->model = new Model_Entrar();
        $this->view = new View();                
    }

    function action_index()
    {                      
        $utility = new Utilities();        
        $errors = array();                
        
        if(!empty($_SESSION['login'])){
            header("Location: /pedidos");   
        }

        $post_array = array(
            // User data
            "login" => "",            
            "password" => ""            
        );

        // Detectamos el metodo post
        if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['entrar']))
        {

            foreach ($post_array as $key => $value)
            {
                if(isset($_POST[$key]) && !empty($_POST[$key]))
                { 
                    $post_array[$key] = $utility->safeInput($_POST[$key]);
                    $this->model->post_data = $post_array;
                } else {
                    $errors[$key] = 'Este campo es obligatorio.';
                }
            }    
            
            if($this->model->check_user($post_array['login'],$post_array['password']) > 0)
            {                
                // Creamos session                    
                $_SESSION["login"] = $post_array['login'];
                header("Location: /pedidos");
            } else {                                
                $this->model->mensaje = 'Usuario no encontrado. Por favor, comprueba los datos.';
            }

            

        }

        // Asignamos vista
        // Asignamos plantilla principal
        // Asignamos datos a la vista                        
        $data = $this->model->get_data();      
        $this->view->generate('entrar.php','main.php',$data);
    }


    function action_salir()
    {        
        Utilities::close_session();             
        header("Location: /");
    }

}