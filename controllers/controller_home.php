<?php

class Controller_Home extends Controller
{
    
    function __construct()
    {
        $this->model = new Model_Home();
        $this->view = new View();                
    }

    function action_index()
    {                                                
        $data = $this->model->get_data();
        $this->view->generate('home.php','main.php',$data);
    }    

}