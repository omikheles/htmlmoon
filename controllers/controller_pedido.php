<?php

class Controller_Pedido extends Controller
{
    
    function __construct()
    {
        $this->model = new Model_Pedido();
        $this->view = new View();                
    }

    function action_index()
    {                      
        $utility = new Utilities();
        $errors = array();
                        
        if(empty($_SESSION['login'])){
        
            $post_array = array(
                // User data
                "nombre" => "",
                "email" => "",
                "direccion" => "",
                "telefono" => "",            
                "login" => "",
                "password" => "",
                "condicion" => "",
                "metodo_pago" => "",
                
                // Otros datos            
                "idcategoria" => "",
                "total_venta" => "",
                "ruta_fichero" => "",
                "comment" => ""        
            );
        
        } else {
            $post_array = array(
                // User data                
                "condicion" => "",                

                // Otros datos            
                "idcategoria" => "",
                "total_venta" => "",
                "ruta_fichero" => "",
                "comment" => ""        
            );
        }

        // Detectamos el metodo post
        if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['pedido']))
        {
            
            foreach ($post_array as $key => $value)
            {
                if(isset($_POST[$key]) && !empty($_POST[$key]))
                { 
                    $post_array[$key] = $utility->safeInput($_POST[$key]);
                    $this->model->post_data = $post_array;
                } else {
                    $errors[$key] = 'Este campo es obligatorio.';
                }
            }

            // Añadimos los articulos                        
            if(empty($_POST['articulos']))
            {
                $this->model->mensaje = 'Por favor, elige un servicio desde el configurador.';
                $errors['servicios'] = 'Elige al menos un servicio desde el configurador';            
            } else {
                $post_array['articulos'] = $_POST['articulos'];
                $this->model->post_data = $post_array;
            }
                                                
            // print_r();                        
            // die();            

            // Comprobamos si quedan errores en el formulario, a contrario procedemos insertar datos a BD            
            if(empty($errors))
            {
            
                // Creamos el usuario
                if(empty($_SESSION['login'])){
                    if($this->model->create_user()){
                                        
                        // Creamos session                    
                        $_SESSION["login"] = $post_array['login'];
                        $_SESSION["online"] = 1;                                                                            
        
                    }
                }

                // TODO
                // Creamos el pedido en la tabla de venta
                if($this->model->create_venta()){                    
                    echo $this->model->create_detalle_venta();
                    // Redirigimos el usuario
                    header("Location: /pedidos");
                }

                // TODO
                // Creamos los detalles en la tabla de detalle venta
                

                
                
            } else {
                // Asignamos los errores
                $this->model->errors = $errors;
            }
        }

        // Asignamos vista
        // Asignamos plantilla principal
        // Asignamos datos a la vista                        
        $data = $this->model->get_data();        
        $this->view->generate('pedido.php','main.php',$data);
    }


    function action_salir()
    {        
        Utilities::close_session();
        header("Location: /");
    }

}