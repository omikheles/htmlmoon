<?php

class Controller_Pedidos extends Controller
{
    
    function __construct()
    {
        $this->model = new Model_Pedidos();
        $this->view = new View();                
    }

    function action_index()
    {                      
        $utility = new Utilities();        
        $errors = array();

        if(empty($_SESSION['login'])){
            header("Location: /");    
        }
        
        // Asignamos vista
        // Asignamos plantilla principal
        // Asignamos datos a la vista                        
        $data = $this->model->get_data();      
        $this->view->generate('pedidos.php','main.php',$data);
    }


    function action_salir()
    {        
        Utilities::close_session();             
        header("Location: /");
    }

}