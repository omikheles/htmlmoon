<?php

class Database {
    
    // Conectamos y selecionamos DB
    public function connect()
    {
        $mysqli = new mysqli(DB_HOST, DB_USER, DB_PASSWORD,DB_NAME);
        if($mysqli->connect_errno > 0){
            die('<br/>No se peude conectar a servidor MySQL [' . $mysqli->connect_error . ']');
        }               
        $mysqli->set_charset("utf8");          
        return $mysqli;
    }    

    // Llamada a DB
    public function query($sql)
    {        
        $db = $this->connect();                        
        $result = $db->query($sql);        
        return $result;
    }

    public function select($sql)
    {
        $rows = array();
        $result = $this->query($sql);
        
        if($result === false)
        {
            return false;
        }

        while($row = $result->fetch_assoc())
        {
            $rows[] = $row;
        }
        return $rows;
    }
    

}