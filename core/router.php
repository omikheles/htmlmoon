<?php
class Router
{

    public function start(){
        
        // Controllador y accion por defecto
        $controller_name = 'Home';
        $action_name = 'index';
        $routes = explode('/',strtok($_SERVER["REQUEST_URI"],'?'));
        // $routes = explode('/', $_SERVER["REQUEST_URI"]);

        // Nombre de controlador
        if( !empty($routes[1]) )
        {
            $controller_name = $routes[1];        
        }
        
        $model_name = $controller_name;

        // Nombre de accion
        if( !empty($routes[2]) )
        {
            $action_name = $routes[2];
        }        

        // Prefijos
        $model_name = 'model_'.$controller_name;
        $controller_name = 'controller_'.$controller_name;        
        $action_name = 'action_'.$action_name;
        
        // Conectamos el fichero de modelo
        $model_file = PATH.'/models/'.strtolower($model_name).'.php';
        
        
        // Conectamos el fichero de controlador
        $controller_file = PATH.'/controllers/'.strtolower($controller_name).'.php';
        if(file_exists($model_file)){
            require_once($model_file);
        }
        
        // echo $controller_file;
        // exit;

        // Comprobamos si existe el fichero        
        if(file_exists($controller_file)){
            require_once($controller_file);
        } else {                                    
            Router::error404();
        }

        // Creamos el controlador
        $controller = new $controller_name;
        $action = $action_name;
        
        if(method_exists($controller,$action)){
            $controller->$action();
        } else {
            Router::error404();
        }

    }

    public function error404()
    {                
        $host = 'http://'.$_SERVER['HTTP_HOST'].'/';
        header('HTTP/1.1 404 Not Found');
		header("Status: 404 Not Found");
        header('Location:'.$host.'404');
        die();
    }

    
}