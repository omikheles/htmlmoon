<?php 

class Utilities
{

    function __construct()
    {
        session_start();
    }

    public function safeInput($field,$type = '')
    {
        
        $field = strip_tags($field);
        $field = trim($field);
        $field = stripslashes($field);
        $field = htmlspecialchars($field);        

        switch($type)
        {
            case 'nospaces':            
                $field = preg_replace('/\s+/', '', $field);
            break;                        
            case 'password':
                $field = md5(real_escape_string($field));
            break;            
        }

        return $field;
    }    

    public function close_session()
    {        
        session_start();
        $_SESSION = array();
        session_destroy();
    }
  

    public function token(){
        $token = md5(uniqid(rand(), TRUE));
        return $token;
    }

}