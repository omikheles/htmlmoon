

    <footer class="footer">
            <div class="pt-3 pb-3">

                    <nav class="navbar navbar-dark bg-transparent navbar-expand-lg">
                        
                        <a class="navbar-brand mr-5" href="#"><img src="/assets/img/htmlmoon.logo.svg" width="100px" /></a>
                        
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                        
                        <div class="collapse navbar-collapse" id="navbarText">
                            <ul class="navbar-nav mr-auto">
                                <li class="nav-item"><a class="nav-link scrollto" href="/">Inicio</a></li>
                                <li class="nav-item"><a class="nav-link scrollto" href="#nosotros">Sobre nosotros</a></li>
                                <li class="nav-item"><a class="nav-link scrollto" href="#servicios">Servicios</a></li>
                                <li class="nav-item"><a class="nav-link scrollto" href="#contactar">Contactar</a></li>
                            </ul>
                            <div class="footer__credits text-right">
                                    <b>Desarrollado por:</b><br/>
                                    Oleg Mikheles.<br/><br/>
                                    Proyecto Idea de empresa / entidad, 2018
                            </div>
                        </div>
        
                    </nav>
        
            </div>
    </footer>

    
    <!-- JavaScripts -->
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <script src="/assets/js/main.js"></script>
    
  </body>
</html>