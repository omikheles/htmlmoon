<?php 

// if($data['page'] == 'home')
// {
//     echo 'Pagina home';
//     echo $data['name'];
// } else {
//     echo 'Otra pagina';
// }

?>

<!doctype html>
<html lang="es">
  <head>
    
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <!-- <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet"> -->
    <link href="https://fonts.googleapis.com/css?family=Ubuntu:300,400,700" rel="stylesheet">
    <link rel="stylesheet" href="/assets/css/main.css">

    <title>HTMLMOON - PSD2HTML</title>
  </head>
  <body class="<?php echo $data['page'];?>">

    <div class="hero">

        <header class="cabecera pt-3 pb-3">

            <nav class="navbar navbar-dark bg-transparent navbar-expand-lg">
                
                <a class="navbar-brand mr-5" href="/"><img src="/assets/img/htmlmoon.logo.svg" width="100px" /></a>
                
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                
                <div class="collapse navbar-collapse" id="navbarText">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item active--off"><a class="nav-link scrollto" href="/">Inicio</a></li>
                        <li class="nav-item"><a class="nav-link scrollto" href="/#nosotros">Sobre HTMLMOON</a></li>
                        <li class="nav-item"><a class="nav-link scrollto" href="/pedido">Nuestros servicios</a></li>
                        <li class="nav-item"><a class="nav-link scrollto" href="/#contactar">Contactar</a></li>
                    </ul>
                    <div class="language">
                        <?php if(empty($_SESSION['login'])): ?>
                            <a href="/entrar">Iniciar sesión</a>
                        <?php else: ?>
                            <span>Bienvenido <?php echo $_SESSION['login']; ?></span>
                            <a href="/pedidos">Mis pedidos</a>
                            <a href="/pedidos/salir">Cerrar sesión</a>
                        <?php endif; ?>
                    </div>
                </div>

            </nav>

        </header>

        <div class="container">
            <div class="row">
                <div class="col text-center">             
                    
                    <?php if($data['page'] == 'home'): ?>

                    <img class="hero__logo mb-3" src="/assets/img/htmlmoon.logo.svg" />                    
                    <div class="hero__micro-title mb-3">PSDs to HTML, Workshop de coding</div>
                    <div class="hero__title mb-5">Convertimos vuestros diseños a código de calidad!</div>                    
                    <a href="/pedido" class="button scrollto">Realizar el pedido</a>
                    <?php else: ?>                    
                    <div class="text-center mb-3"><img src="<?php echo $data['meta']['icon']; ?>" width="50" /></div>
                    <?php if(!empty($data['user_name']) && $data['page'] == 'pedidos'): ?>
                    <div class="hero__micro-title mb-3">Bienvenido <?php echo $data['user_name']; ?></div>
                    <?php endif; ?>
                    <div class="hero__title"><?php echo $data['meta']['title']; ?></div>
                    <?php endif; ?>

                </div>
            </div>
        </div>

    </div>