<?php

$whitelist = array(
    '127.0.0.1',
    '::1'
);

// Ruta
define('PATH', realpath(dirname(__FILE__)));

// Includes principales
define ( 'HEADER', PATH.'/includes/header.php' );
define ( 'FOOTER', PATH.'/includes/footer.php' );

if(in_array($_SERVER['REMOTE_ADDR'], $whitelist)){
    // Base de datos local
    define ( 'DB_HOST', 'localhost' );
    define ( 'DB_USER', 'root' );
    define ( 'DB_PASSWORD', '' );
    define ( 'DB_NAME', 'dbsistema' );
} else {
    // Base de datos servidor 000webhost
    // --------------------------------------
    define ( 'DB_HOST', 'localhost' );
    define ( 'DB_USER', 'id7749583_uocroot' );
    define ( 'DB_PASSWORD', '%056ID\5*Sz^' );
    define ( 'DB_NAME', 'id7749583_htmlmoon' );
}

// El motor
require_once(PATH.'/core/model.php');
require_once(PATH.'/core/view.php');
require_once(PATH.'/core/controller.php');
require_once(PATH.'/core/utilities.php');
require_once(PATH.'/core/db.php');

// Enrutador
require_once(PATH.'/core/router.php');
$router = new Router;
$router->start();
// Router::start();