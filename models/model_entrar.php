<?php

class Model_Entrar extends Model
{               
    public $errors;
    public $post_data;
    public $mensaje;

    private $db;
    private $connect;

    function __construct()
    {        
        $this->db = new database();        
        $this->connect = $this->db->connect();
    }   
    
    function check_user($login = '',$password = ''){
        
        $password = $this->hash_password($password);

        $query = "SELECT * FROM usuario WHERE login = '$login' AND password= '$password'";
        $data = $this->db->query($query);
        return $data->num_rows;
    }

    function hash_password($pwd)
    {
        if($pwd != '')
        return hash("SHA1",$pwd);
    }

    function get_data($args = '')
    {        

        return array(
            'page' => 'entrar',
            'meta' => array(
                'title'=> 'Iniciar sesión',
                'icon' => '/assets/img/user.svg'
            ),
            'errors' => $this->errors,
            'post_data' => $this->post_data,
            'mensaje' => $this->mensaje    
        );
    }

}