<?php

class Model_Home extends Model
{           
        
    function get_data()
    {                    
        $nombre = '';
        $utility = new Utilities();        
        if(!empty($_GET['nombre']))
        {
            $nombre = $utility->safeInput($_GET['nombre']);
        }        

        $db = new database();        
        $connect = $db->connect();
        $categories = $db->select("SELECT * FROM categoria");

        // Cerramos conexion      
        $connect->close();
        
        // Datos de la pagina
        return array(
            'page' => 'home',
            'name' => $nombre,
            'meta' => array(
                'title'=> 'Home title yes'
            ),
            'categories' => $categories
        );
    }

}