<?php

class Model_Pedido extends Model
{               
    public $errors;
    public $post_data;
    public $mensaje;

    private $db;
    private $connect;

    function __construct()
    {        
        $this->db = new database();        
        $this->connect = $this->db->connect();
    }

    function create_user($args = '')
    {
        $data = $this->post_data;
        
        $password = $data['password'];
        $password = $this->hash_password($password);
        
        $nombre = $data['nombre'];
        $direccion = $data['direccion'];
        $telefono = $data['telefono'];
        $email = $data['email'];
        $login = $data['login'];
        $metodo_pago = $data['metodo_pago'];
        
        if($this->check_usuario_existe($data['login']) == 0)
        {
            
            // $query = "INSERT INTO `dbsistema`.`usuario` (`nombre`, `direccion`, `telefono`, `email`, `login`, `password`, `condicion`) VALUES ('Mike', 'Sol', '600000000', 'mike@gmail.com', 'duper', '123123', '1')";

            $query = "INSERT INTO `usuario` (`nombre`, `direccion`, `telefono`, `email`, `login`, `password`, `condicion`, `metodo_pago`) VALUES ('$nombre', '$direccion', '$telefono', '$email', '$login', '$password', '1', '$metodo_pago');";            
        
            if($this->db->query($query))
            {
                return true;
            } else {
                $this->mensaje = 'Lo sentimos no hemos podido crear el usuario. Revisa los datos.';
                return false;
            }

        } else {
            $this->mensaje = 'Lo sentimos el usuario ya existe. Elige otro nombre.';
            return false;            
        }                          

    }

    function create_venta($args = '')
    {
        $data = $this->post_data;

        if(!empty($_SESSION['login'])){
            $user = $this->get_user($_SESSION['login']);
            $user_id = $user[0]['idusuario'];
        }
         
        $total_venta = $data['total_venta'];
        $ruta_fichero = $data['ruta_fichero'];
        $comentarios = $data['comment'];        

        date_default_timezone_set('Europe/Madrid');
        $date = new DateTime();
        $unix_timestamp = $date->getTimestamp();

        $query = "INSERT INTO `venta` (`idusuario`, `fecha_hora`, `impuesto`, `total_venta`, `estado`, `ruta_fichero`, `comentario`) VALUES ('$user_id', FROM_UNIXTIME($unix_timestamp), '0', '$total_venta', 'Pendiente de pago', '$ruta_fichero', '$comentarios')";

        // INSERT INTO `dbsistema`.`venta` (`idusuario`, `fecha_hora`, `impuesto`, `total_venta`, `estado`, `ruta_fichero`, `comentario`) VALUES ('2', FROM_UNIXTIME(1545505510), '0', '290', '', 'https://tpc.googlesyndication.com/simgad/6315776724983511804', 'No words');            

        if($this->db->query($query))
        {
            return true;
        } else {
            $this->mensaje = 'Lo sentimos no hemos podido crear el pedido. Revisa los datos y intenta otra vez.';
            return false;
        }
        
        
    }

    function create_detalle_venta(){        
        
        $data = $this->post_data;

        if(!empty($_SESSION['login'])){
            $user = $this->get_user($_SESSION['login']);
            $user_id = $user[0]['idusuario'];
        }
        
        $last_order_query = "SELECT * FROM venta WHERE idusuario = '$user_id' order by idventa desc limit 1";
        $last_order_id = $this->db->select($last_order_query)[0]['idventa'];

        $articulos = $data['articulos'];
                
        foreach ($articulos as $key) {

            $query = "INSERT INTO `detalle_venta` (`idventa`, `idarticulo`, `cantidad`, `precio_venta`, `descuento`) VALUES ('$last_order_id', '$key', '1', '0', '0')";
            $this->db->query($query);
            
        }

    }

    function get_categories()
    {
        return $this->db->select("SELECT * FROM categoria");
    }

    function get_articulos()
    {
        return $this->db->select("SELECT * FROM articulos");
    }

    function get_user($usuario = '')
    {        
        $query = "SELECT * FROM usuario WHERE login = '$usuario'";
        return $this->db->select($query);
    }

    function check_usuario_existe($usuario = ''){
        $query = "SELECT * FROM usuario WHERE login = '$usuario'";
        $data = $this->db->query($query);         
        return $data->num_rows;         
    }      

    function hash_password($pwd)
    {
        if($pwd != '')
        return hash("SHA1",$pwd);
    }

    function get_data()
    {                                           
        
        $username = $title = '';
        
        // Titulo de la pagina para usuario nuevo y antiguo
        if(!empty($_SESSION['login'])){
            $title = 'Realizar otro pedido';
        } else {
            $title = 'Realizar el pedido y crear mi cuenta personal';
        }        

        return array(
            'page' => 'pedido',
            'meta' => array(
                'title'=> $title,
                'icon' => '/assets/img/order.svg'
            ),
            'errors' => $this->errors,
            'post_data' => $this->post_data,
            'mensaje' => $this->mensaje,
            'categories' => $this->get_categories(),
            'articulos' => $this->get_articulos()
        );
    }

}