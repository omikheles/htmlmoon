<?php

class Model_Pedidos extends Model
{               
    public $errors;
    public $post_data;
    public $mensaje;

    private $db;
    private $connect;

    function __construct()
    {        
        $this->db = new database();        
        $this->connect = $this->db->connect();
    }   

    function get_ventas()
    {   
        if(!empty($_SESSION['login'])){
            $user = $this->get_user($_SESSION['login']);
            $user_id = $user[0]['idusuario'];
        }        
        return $this->db->select("SELECT * FROM venta WHERE idusuario='$user_id'");
    }    

    function get_user($usuario = '')
    {        
        $query = "SELECT * FROM usuario WHERE login = '$usuario'";
        return $this->db->select($query);
    }

    function get_data($args = '')
    {

        $username = $title = '';        

        return array(
            'page' => 'pedidos',
            'meta' => array(
                'title'=> 'Mis Pedidos',
                'icon' => '/assets/img/list.svg'
            ),
            'errors' => $this->errors,
            'orders' => $this->get_ventas(),
            'user_name' => $this->get_user($_SESSION['login'])[0]['nombre']
        );
    }

}