$(function(){        

    $('.scrollto').on('click',function(){
        var el = $(this).attr('href');
        $([document.documentElement, document.body]).animate({
            scrollTop: $(el).offset().top
        }, 1500);        
    });

    // Filtramos los articulos segun categoria seleciona
    // Sacamo el total del pedido para previsualizar
    (function(){

        var $radios_categoria = $('input:radio[name=idcategoria]');
        if($radios_categoria.is(':checked') === false) {
            $radios_categoria.filter('[value=1]').prop('checked', true);
        }
        
        filter_services(1);

        $radios_categoria.on('click',function(){             
            $('#list-articulos input:checkbox').prop('checked', false);
            filter_services($(this).val());
        });        

        $('#list-articulos input[type=checkbox]').on('change',function(){            
            total_order();
            $('#total-box').removeClass('d-none');
        });
        
        $radios_categoria.on('change',function(){                        
            total_order();
        });

        function filter_services($cat){
            $('#list-articulos li').each(function(i){

                var $obj = $(this);

                if($(this).data('category') == $cat){
                    $obj.removeClass('d-none');
                } else {
                    $obj.addClass('d-none');
                }                

            });
            setTimeout(function(){
                $('#list-articulos').removeClass('d-none');
            },500);
            
        }
        
        // Realizamos el calculo total de pedido
        function total_order(){
            
            var total_amount = 0;
            var items = new Array();
            $('#total_venta').val(0);
            $items_list = $('.total-box__list').html('');
            $items_amount = $('.total-box__amount').html('');
            
            
            $('#list-articulos input[type=checkbox]:checked').each(function(i){            
                var $obj = $(this);
                total_amount += parseInt($obj.data('price'));
                items.push($obj.data('price')+'&euro; - '+$obj.data('service'));
            });

            $('#total_venta').val(total_amount);
            $items_amount.html('<span>Total:</span> '+total_amount+'&euro;');
            
            for (i = 0; i < items.length; ++i) {
                console.log(items[i]);
                $items_list.append('<div class="total-box__list-item">'+items[i]+'</div>');
            }

        }

    })();


    // Enviamos el formulario de contacto
    (function(){                

        var form = $('#contact-form');

        form.on('submit',function(e){

            e.preventDefault();            

            $.ajax({
                type: 'POST',
                url: "/send",
                data: form.serialize(),
                cache: false,
                success: function(data){                        
                },
                statusCode: {
                    200: function(){
                        form.addClass('d-none');
                        $('.result').removeClass('d-none');
                    },
                    404: function(){
                        console.log('something is wrong');
                    }
                }
            });

        });

    })();
    

});