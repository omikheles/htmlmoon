<section class="p-5">
    <div class="container">
        <div class="row">

            <div class="col">
        
                <form class="order-form" method="POST">

                    <div class="order-form__section p-5 mb-5">

                        <h3 class="decorate decorate--left mb-4">Entrar como usuario registrado</h3>

                        <?php echo '<div class="mensaje mb-3">'.$data['mensaje'].'</div>'; ?>

                        <div class="form-row">
                            <div class="form-group col-md-6">                                    
                                <input type="text" class="form-control" id="login" name="login" placeholder="Nombre usuario" value="<?php echo $data['post_data']['login']; ?>" required>
                            </div>
                            <div class="form-group col-md-6">                                    
                                <input type="password" class="form-control" id="password" name="password" placeholder="Contraseña" required>           
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-6 text-right">
                            </div>
                            <div class="form-group col-md-6 text-right">
                                <button type="submit" class="button" name="entrar">Entrar</button>
                            </div>
                        </div>

                    </div>
                    
                </form>

            </div>

        </div>
    </div>
</section>

