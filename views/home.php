<section class="p-5" id="nosotros">
        <div class="container">
            <div class="row">
                <div class="col">
                    <h1 class="decorate text-center mb-5">Quiénes <b>somos</b></h1>
                    <div class="row align-items-center mb-5">
                        <div class="col-12 col-md-6 mb-5 mb-md-0">
                            <p><b>Somos una tienda online de servicios de coding formado por el grupo de los desarrolladores apasionados por el código.</b></p>

                            <p class="mb-5">Nuestro producto principal está enfocado en recibir su diseño, preparado en formato PSD o SKETCH y convertirlo a HTML perfectamente maquetado, adaptado para móviles con las últimas técnicas de desarrollo online.</p>
                            <a href="#servicios" class="button scrollto">Solicitar el servicio</a>
                        </div>
                        <div class="col-12 col-md-6">
                            <a href="https://www.youtube.com/embed/DZUQUqYKIvQ" target="_blank"><img src="/assets/img/video.jpg" class="img-full" /></a>
                        </div>
                    </div>                    
                    
                    <div class="row mb-5">
                        <div class="col-12 mb-5">
                            <h2 class="decorate text-center">La idea y el proceso <b>es simple</b></h2>
                            <p class="text-center">Convertimos vuestros diseños a código de calidad!</p>
                        </div>
                        <div class="col-12 text-center">

                            <img src="/assets/img/idea.jpg" class="img-full" style="max-width:80%;" />

                        </div>                      
                    </div>                    

                </div>
            </div>            
        </div>
    </section>

    <section class="servicios p-5" id="servicios">
        <div class="container">
            <div class="row">
                <div class="col text-center">
                    <h1 class="decorate text-center mb-5">Nuestros <b>servicios destacados</b></h1>
                    <p>Los servicios que ofrecemos son los siguientes:</p>
                </div>
            </div>
            <div class="row align-items-center mb-5">
                <div class="col-12">
                <div class="order-categories d-md-flex justify-content-between" style="">
                        
                        <?php foreach ($data['categories'] as $key) { ?>
                        
                        <div class="order-category">                                  
                            <input type="radio" name="idcategoria" id="category-<?php echo $key['idcategoria'] ?>" value="<?php echo $key['idcategoria'] ?>">
                            <label for="category-<?php echo $key['idcategoria'] ?>">
                            <img src="/assets/img/category-<?php echo $key['idcategoria'] ?>.svg" class="order-category__icon" height="50" />
                            <span class="order-category__title"><?php echo $key['nombre'] ?></span>
                            </label>
                        </div>                            

                        <?php } ?>                        

                        </div>
                </div>                
            </div>
            <div class="row">
                <div class="col text-center">
                    <a href="/pedido" class="button button--light">Realizar el pedido</a>
                </div>
            </div>
        </div>
    </section>

    <section class="p-5" id="contactar">
        <div class="container">
            <div class="row mb-2">
                <div class="col text-center">
                    <h1 class="decorate text-center mb-5">Contactar con <b>HTMLMOON</b></h1>
                    <p class="text-lg"><b>¡Estaremos encantados hablar sobre vuestro próximo proyecto!</b></p>
                    <p>Puede contactar con nosotros a través de estos medios:</p>                 
                </div>                                
            </div>

            <div class="row">
                <div class="col">
                    <div class="text-center d-none result">
                        <img src="/assets/img/checked.svg" width="100" />
                    </div>
                    <form id="contact-form" class="contact-form" action="send.php" method="POST">
                        <div class="form-row">
                            <div class="form-group col-md-6">                                    
                                <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Nombre">                                    
                            </div>
                            <div class="form-group col-md-6">                                    
                                <input type="email" class="form-control" id="email" name="email" placeholder="Email">
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col">  
                                <textarea class="form-control" id="comment" name="comment" rows="3"></textarea>
                            </div>                                
                        </div>
                        <div class="form-row"> 
                            <div class="form-group col text-right">
                                <button type="submit" class="button">Enviar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </section>