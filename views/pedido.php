<?php        
    $errors = $data['errors'];
    // if(isset($_SESSION['login']) && !empty($_SESSION['login'])) {
    //     echo 'not empty';
    // } else {
    //     echo 'empty';
    // }
    
    // print_r($data['post_data']);
    // foreach ($data['categories'] as $key) {
    //     echo $key['idcategoria'].'-'.$key['nombre'].'-'.$key['descripcion'].'<br/>';
    // }

    // foreach ($data['articulos'] as $key) {
    //     if($key['idcategoria'] == 1){
    //         echo $key['nombre'].'<br/>';
    //     }
    // }
    
    // print_r($data['post_data']['articulos']);
    // if(in_array('2',$data['post_data']['articulos'])){ echo 'checked'; };
    // echo array_search('2',$data['post_data']['articulos']);
?>

<section class="p-5">
    <div class="container">
        <div class="row">

            <div class="col">

                <?php if(!empty($errors)): ?>
                <div class="errors mb-5 text-center">
                <h4 class="mb-4">Por favor, revisa el formulario.</h4>                
                    <?php                                            
                        // Mensaje
                        echo '<div class="mensaje mb-3">'.$data['mensaje'].'</div>';
                        
                        // Errores de formulario
                        foreach ($errors as $key => $value) {
                            echo '<div class="errors__error"><span>'.$key.'</span> - '.$value.'</div>';
                        }
                    ?>
                </div>
                <?php endif; ?>
                
                <form class="order-form" method="POST">
                    
                    <input type="hidden" id="total_venta" name="total_venta" value="" />
                    
                    <p class="text-center mb-4">Elige el tipo de servicio que busca.</p>

                    <div class="order-categories mb-5 d-md-flex justify-content-between">
                        
                        <?php foreach ($data['categories'] as $key) { ?>
                        
                        <div class="order-category">                                  
                            <input type="radio" name="idcategoria" id="category-<?php echo $key['idcategoria'] ?>" value="<?php echo $key['idcategoria'] ?>">
                            <label for="category-<?php echo $key['idcategoria'] ?>">
                            <img src="/assets/img/category-<?php echo $key['idcategoria'] ?>.svg" class="order-category__icon" height="50" />
                            <span class="order-category__title"><?php echo $key['nombre'] ?></span>
                            </label>
                        </div>                            

                        <?php } ?>                        

                    </div>
                    
                    <div class="order-form__section p-5 mb-5">
                        <div id="configurator" class="configurator">                    
                    
                            <h3 class="decorate decorate--left mb-4">Configurador / selección de los servicios</h3>          

                            <!-- <div class="form-row">
                                <div class="form-group col-md-6">                                    
                                    <input type="text" class="form-control" id="numpaginas" name="numpaginas" placeholder="Número de páginas" value="<?php echo $data['post_data']['nombre']; ?>" required>
                                    <small class="form-text text-muted">Indica la cantidad de las páginas para maquetar. El precio es por pagina.</small>
                                </div>                    
                            </div>        -->

                            <div id="list-articulos" class="list-articulos d-none">
                                <ul>

                                    <?php                                    

                                    foreach ($data['articulos'] as $key)
                                    {

                                    ?>
                                    <li data-category="<?php echo $key['idcategoria']; ?>">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" class="custom-control-input" id="articulo-<?php echo $key['idarticulo']; ?>" name="articulos[]" value="<?php echo $key['idarticulo']; ?>" data-price="<?php echo $key['precio']; ?>" data-service="<?php echo $key['nombre']; ?>">
                                            <label class="custom-control-label d-flex" for="articulo-<?php echo $key['idarticulo']; ?>">
                                                <span class="list-articulos__title flex-grow-1"><em><?php echo $key['nombre']; ?></em></span>
                                                <span class="list-articulos__price"><?php echo $key['precio']; ?>&euro;</span>
                                            </label>
                                        </div>
                                    </li>

                                    <?php                                                                            

                                    }

                                    ?>

                                </ul>
                            </div>
                            
                            <?php

                                // foreach ($data['articulos'] as $key) {
                                //     if($key['idcategoria'] == 1){
                                //         echo $key['nombre'].'<br/>';
                                //     }
                                // }

                            ?>

                        </div>
                    </div>

                    <div id="total-box" class="order-form__section total-box p-5 mb-5 d-none">                        
                        <div class="row">
                            <div class="col-md-12">
                                <div><b>El resumen de pedido</b></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 total-box__list"></div>
                            <div class="col-md-6 total-box__amount text-right"></div>
                        </div>
                    </div>

                    <div class="order-form__section p-5 mb-5">
                    
                    <?php
                    if(!isset($_SESSION['login']) && empty($_SESSION['login'])):
                    //if(empty($_SESSION['online']) && empty($_SESSION['login'])): 
                    ?>

                    
                    <h3 class="decorate decorate--left mb-4">Datos personales</h3>

                    <div class="form-row">
                        <div class="form-group col-md-6">                                    
                            <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Nombre" value="<?php echo $data['post_data']['nombre']; ?>" required--off>
                        </div>
                        <div class="form-group col-md-6">                                    
                            <input type="email" class="form-control" id="email" name="email" placeholder="Email" value="<?php echo $data['post_data']['email']; ?>" required--off>           
                        </div>
                    </div>                              
                    <div class="form-row">
                        <div class="form-group col-md-12">                                    
                            <input type="text" class="form-control" id="direccion" name="direccion" placeholder="Dirección física" value="<?php echo $data['post_data']['direccion']; ?>" required--off>
                        </div>
                    </div>        
                    <div class="form-row">
                        <div class="form-group col-md-6">                                    
                            <input type="text" class="form-control" id="telefono" name="telefono" placeholder="Teléfono" value="<?php echo $data['post_data']['telefono']; ?>" required--off>
                        </div>
                        <div class="form-group form-group--select col-md-6">                                                            
                            <select class="form-control" id="metodo_pago" name="metodo_pago" required--off> 
                                <option value="">Metodo de pago</option>
                                <option value="PAYPAL"<?php if($data['post_data']['metodo_pago'] =='PAYPAL'){echo ' selected';} ?>>PAYPAL</option>
                                <option value="CREDIT CARD"<?php if($data['post_data']['metodo_pago'] =='CREDIT CARD'){echo ' selected';} ?>>Tarjeta de crédito</option>
                            </select>
                        </div>
                    </div>                                    
                    <div class="form-row">
                        <div class="form-group col-md-6">                                    
                            <input type="text" class="form-control" id="login" name="login" placeholder="Usuario" value="<?php echo $data['post_data']['login']; ?>" required--off>
                        </div>
                        <div class="form-group col-md-6">                                    
                            <input type="password" class="form-control" id="password" name="password" placeholder="Contraseña" required--off>
                        </div>
                    </div>

                    <?php endif; ?>

                    <div class="form-row">
                        <div class="form-group col">  
                            <textarea class="form-control" id="comment" name="comment" rows="3" placeholder="Comentarios sobre el proyecto..." required--off><?php echo $data['post_data']['comment']; ?></textarea>
                        </div>                                
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-12">                                    
                            <input type="text" class="form-control" id="ruta_fichero" name="ruta_fichero" placeholder="Enlace para el fichero PSD" value="<?php echo $data['post_data']['ruta_fichero']; ?>" required--off>
                        </div>                        
                    </div>                    

                    <div class="form-row align-items-center"> 
                        <div class="form-group col-md-6">                        
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="condicion" name="condicion" value="1" required--off>
                                <label class="custom-control-label" for="condicion">Estoy de acuerdo con los términos y condiciones.</label>
                            </div>
                        </div>
                        <div class="form-group col-md-6 text-right">
                            <button type="submit" class="button" name="pedido">Enviar</button>
                        </div>
                    </div>                    

                    </div>

                </form>

            </div>

        </div>
    </div>
</section>

