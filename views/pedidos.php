<section class="p-5">
    <div class="container">
        <div class="row">

            <div class="col">
        
                <p class="text-center">Pedidos realizados. <a href="/pedido">Realizar nuevo pedido.</a></p>                                

                <div class="row">                                

                    <?php foreach ($data['orders'] as $key) { ?>

                    <div class="col-12 mb-4">
                        <div class="card order">  
                            <div class="card-header d-flex">
                                <div class="mr-auto">Pedido #<?php echo $key['idventa']; ?></div>
                                <div class="order__date"><b>Estado:</b> en desarrollo</div>
                            </div>
                            <div class="card-body d-md-flex justify-content-between align-items-center">
                                <div>
                                    <small>Precio total en euros</small>
                                    <h5 class="card-title mb-0"><?php echo $key['total_venta']; ?>&euro;</h5>
                                </div>
                                <div style="max-width: 350px">
                                    <p class="card-text mb-0"><?php echo $key['comentario']; ?></p>
                                    <b>Fecha realizado:</b> <?php echo $key['fecha_hora']; ?>
                                </div>
                                <a href="#" class="button button--full"><?php echo $key['estado']; ?></a>
                            </div>
                        </div>
                    </div>
                    
                    <?php } ?>
                    
                </div>

                <?php //print_r($data['orders']); ?>

            </div>

        </div>
    </div>
</section>

